export const OAuthSettings = {
    appId: '3c439c13-bc8c-451d-a63c-c79ba84a70f2',
    redirectUri: 'https://smart-interview-scheduler.herokuapp.com/',
    scopes: [
      "user.read",
      "mailboxsettings.read",
      "calendars.readwrite"
    ]
  };